%define debug_package %{nil}

Name:       chatglm-cpp
Version:    0.2.4
Release:    5
License:    MIT
Summary:    Port of Chinese lagre model ChatGLM-6B & ChatGLM2-6B implemented based on C/C++

URL:            https://github.com/li-plus/chatglm.cpp
Source0:        https://github.com/li-plus/chatglm.cpp/releases/download/0.2.4/chatglm-cpp-%{version}.tar.gz

%ifarch loongarch64
Patch0:        0001-Fix-build-error-for-loongarch64.patch
%endif

BuildRequires:  gcc,gcc-c++,cmake
Requires:       sentencepiece

%description
Port of Chinese lagre model ChatGLM-6B & ChatGLM2-6B implemented based on C/C++,
it can be used for model dialogue based on local laptops.

%prep
%autosetup -b 0 -n %{name}-%{version} -p1

%build
mkdir chatglm_builddir
pushd chatglm_builddir
cmake ..
%make_build
popd

%install
pushd chatglm_builddir
%make_install
install bin/main  %{buildroot}%{_prefix}/local/bin/chatglm_cpp_main
install lib/libchatglm.a  %{buildroot}%{_prefix}/local/bin/libchatglm.a
install ../chatglm_cpp/convert.py %{buildroot}%{_prefix}/local/bin/chatglm_convert.py
mv %{buildroot}%{_prefix}/local/*  %{buildroot}%{_prefix}

#Remove files from package sentencepiece.
find %{buildroot}%{_prefix} -type f -name "*sentencepiece*" -exec rm -f {} +
rm %{buildroot}%{_bindir}/spm_* -f
popd

%files
%{_bindir}/*
%{_libdir}/*
%{_includedir}/*
%{_datadir}/pkgconfig/ggml.pc
/usr/lib/static/libggml.a

%changelog
* Fri May 17 2024 zhangzikang <zhangzikang@kylinos.cn> - 0.2.4-5
- Fix build error for loongarch64

* Wed Sep 20 2023 zhoupengcheng <zhoupengcheng11@huawei.com> - 0.2.4-4
- packing chatglm_convert.py file
- install python models for chatglm_convert.py int dockerfile
- update long-term yum.repo in dockerfile

* Tue Sep 19 2023 zhoupengcheng <zhoupengcheng11@huawei.com> - 0.2.4-3
- add dokerfile

* Wed Sep 6 2023 zhoupengcheng <zhoupengcheng11@huawei.com> - 0.2.4-2
- Fix the conflict with the installation of package sentencepiece.

* Tue Aug 22 2023 zhoupengcheng <zhoupengcheng11@huawei.com> - 0.2.4-1
- Init package
